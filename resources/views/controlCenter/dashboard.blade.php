@extends('layouts.contenu')

@section('title', 'Control Center - Dashboard')

@section('content')
  <div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
      </ol>
    </div>

    <div class="row mb-3">
      <!-- Earnings (Monthly) Card Example -->
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card h-100">
          <div class="card-body">
            <div class="row align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-uppercase mb-1">Commandes</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">3/10</div>
                <div class="mt-2 mb-0 text-muted text-xs">
                  <span class="text-success mr-2"><i class="fas fa-sync"></i> Mise a jour le</span>
                  <span>01/01/2020</span>
                </div>
              </div>
              <div class="col-auto">
                <i class="fas fa-people-carry fa-2x text-primary"></i>

              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Earnings (Annual) Card Example -->
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card h-100">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-uppercase mb-1">Reservations</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">5/20</div>
                <div class="mt-2 mb-0 text-muted text-xs">
                  <span class="text-success mr-2"><i class="fas fa-sync"></i> Mise a jour le</span>
                  <span>02/01/2020</span>
                </div>
              </div>
              <div class="col-auto">
                <i class="fas fa-edit fa-2x text-success"></i>

              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- New User Card Example -->
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card h-100">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-uppercase mb-1">Sliders</div>
                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">00</div>
                <div class="mt-2 mb-0 text-muted text-xs">
                  <span class="text-success mr-2"><i class="fas fa-sync"></i> Mise a jour le</span>
                  <span>03/01/2020</span>
                </div>
              </div>
              <div class="col-auto">
                <i class="fas fa-sliders-h fa-2x text-info"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Pending Requests Card Example -->
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card h-100">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-uppercase mb-1">Messages</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">2/18</div>
                <div class="mt-2 mb-0 text-muted text-xs">
                  <div class="mt-2 mb-0 text-muted text-xs">
                    <span class="text-success mr-2"><i class="fas fa-sync"></i> Mise a jour le</span>
                    <span>01/01/2020</span>
                  </div>
                </div>
              </div>
              <div class="col-auto">
                <i class="fas fa-comments fa-2x text-warning"></i>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Invoice Example -->
      <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Dernieres Commandes </h6>
            <a class="m-0 float-right btn btn-danger btn-sm" href="#">Voir Plus <i
                class="fas fa-chevron-right"></i></a>
          </div>
          <div class="table-responsive">
            <table class="table align-items-center table-flush">
              <thead class="thead-light">
                <tr>
                  <th>ID</th>
                  <th>Nom Commande</th>
                  <th>Date Livraison</th>
                  <th>Lieu Livraison</th>
                  <th>Status</th>
                  <th>Date D'envoi</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><a href="#">RA0449</a></td>
                  <td>Udin Wayang</td>
                  <td>Nasi Padang</td>
                  <td>Tasi Madang</td>
                  <td><span class="badge badge-success">Delivered</span></td>
                  <td><a href="#" class="btn btn-sm btn-primary">01/01/2020</a></td>
                </tr>
                <tr>
                  <td><a href="#">RA0449</a></td>
                  <td>Udin Wayang</td>
                  <td>Nasi Padang</td>
                  <td>Tasi Madang</td>
                  <td><span class="badge badge-success">Delivered</span></td>
                  <td><a href="#" class="btn btn-sm btn-primary">01/01/2020</a></td>
                </tr>
                <tr>
                  <td><a href="#">RA0449</a></td>
                  <td>Udin Wayang</td>
                  <td>Nasi Padang</td>
                  <td>Tasi Madang</td>
                  <td><span class="badge badge-danger">No Delivered</span></td>
                  <td><a href="#" class="btn btn-sm btn-primary">01/01/2020</a></td>
                </tr>
                <tr>
                  <td><a href="#">RA0449</a></td>
                  <td>Udin Wayang</td>
                  <td>Nasi Padang</td>
                  <td>Tasi Madang</td>
                  <td><span class="badge badge-success">Delivered</span></td>
                  <td><a href="#" class="btn btn-sm btn-primary">01/01/2020</a></td>
                </tr>
                <tr>
                  <td><a href="#">RA0449</a></td>
                  <td>Udin Wayang</td>
                  <td>Nasi Padang</td>
                  <td>Tasi Madang</td>
                  <td><span class="badge badge-danger">No Delivered</span></td>
                  <td><a href="#" class="btn btn-sm btn-primary">01/01/2020</a></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="card-footer"></div>
        </div>
      </div>


  </div>
  <!---Container Fluid-->
  </div>

@endsection

@section('scripts')

@endsection
