@extends('layouts.contenu')

@section('title')
  Control Center - Commande
@endsection

@section('content')

<div class="container-fluid" id="container-wrapper">
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-people-carry"></i> Commande</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
      <li class="breadcrumb-item">commande</li>
    </ol>
  </div>

  <div class="text-center">
    <div class="alert alert-light" role="alert">
      <span  class="btn btn-info btn-sm">
        <i class="fas fa-info-circle"></i>
      Consulter la liste des Commandes.</span>
    </div>


    <div class="row">

      <!-- DataTable with Hover -->
      <div class="col-lg-12">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6>  <button type="button" class="btn btn-outline-danger mb-1 float-md-left commande">
              <i class="fas fa-plus"></i> Voir Commandes éffectués</button></h6>
          </div>

        </div>
      </div>
      <div class="col-lg-12">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Liste des Commandes</h6>
          </div>
          <div class="table-responsive p-3">
            <table class="table align-items-center table-flush table-hover" id="commTable">
              <thead class="thead-light">
                <tr>
                  <th>Commande</th>
                  <th>Client</th>
                  <th>Télèphone</th>
                  <th>Email</th>
                  <th>Livraison</th>
                  <th>Lieu</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Commande</th>
                  <th>Client</th>
                  <th>Télèphone</th>
                  <th>Email</th>
                  <th>Livraison</th>
                  <th>Lieu</th>
                  <th>Action</th>
                </tr>
              </tfoot>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


  </div>


</div>

<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog"
  aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">

      <div class="modal-body">
        <div class="alert alert-danger alert-dismissible" role="alert">

          <h6><i class="fas fa-info"></i><b> Confirmation ...!</b></h6>
          Voulez-vous vraiment supprimer cette commande ??
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary mb-1 float-md-left" data-dismiss="modal">Non</button>

        <button type="button" name="del_button" id="del_button" class="btn btn-outline-danger mb-1 float-md-left">Confirmer</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="ModalCommande" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-people-carry"></i> Commande</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <div  role="alert">
              <span  class="btn btn-info btn-sm">
                <i class="fas fa-info-circle"></i>
                <span id="title">Confirmer la Commande</span>
            </span>
            </div>


          </div>
          <hr>
          <div class="card-body">

            <div class="form-group">
              <span id="nom_com"></span>
            </div>
            <div class="form-group">
              <span id="nom_client"></span>
            </div>
            <div class="form-group">
              <span id="livraison"></span>
            </div>
            <hr>
            <div class="form-group">
              <span id="lieu"></span>
            </div>
            <hr>

            <span id="form_result"></span>

            <form method="post"  id="form_commande" class="form-horizontal">
              @csrf
              <select class="form-control mb-3" name="status" id="status">
                  <option value="1">Confirmer</option>
              </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="action" id="action" value="Add" />
        <button type="button" class="btn btn-outline-danger mb-1 float-md-left" data-dismiss="modal">Close</button>
        <input type="hidden" name="id" id="id" />
        <button type="submit" id="commande_button" class="btn btn-outline-primary mb-1 float-md-left">Confirmer</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="listeModal" tabindex="-1" role="dialog"
  aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">

      <div class="modal-body">

        <div class="row">

          <!-- DataTable with Hover -->
          <div class="col-lg-12">
            <div class="card mb-4">
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Liste des Commandes</h6>
              </div>
              <div class="table-responsive p-3">
                <table class="table align-items-center table-flush table-hover" id="listecommTable">
                  <thead class="thead-light">
                    <tr>
                      <th>Nom</th>
                      <th>Client</th>
                      <th>Télèphone</th>
                      <th>Email</th>
                      <th>le</th>
                      <th>Lieu</th>
                      <th>Modifier</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Nom</th>
                      <th>Client</th>
                      <th>Télèphone</th>
                      <th>Email</th>
                      <th>le</th>
                      <th>Lieu</th>
                      <th>Modifier</th>
                    </tr>
                  </tfoot>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>


    </div>

    <div class="modal-footer">
      <button type="button" class="btn btn-outline-danger mb-1 float-md-left" data-dismiss="modal">Close</button>

    </div>
  </div>
</div>
@endsection


@section('scripts')

  <script type="text/javascript">
  $(document).ready(function(){

  $(function () {

    var id;
    var id = document.getElementById('iduser').value
    console.log(id);
    $('#commTable').DataTable({

    //  $('#user_id').val(id);
        processing: true,
        serverSide: true,
        ajax: "/allCommande/"+id,
        columns: [
            //{data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'nom_com', name: 'nom_com'},
            {data: 'nom_client', name: 'nom_client'},
            {data: 'phone', name: 'phone'},
            {data: 'email', name: 'email'},
            {data: 'livraison', name: 'livraison'},
            {data: 'lieu', name: 'lieu'},
          //  {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

  });

  var user_id;
  $(document).on('click', '.delete', function(){
  user_id = $(this).attr('id');

  $('#confirmModal').modal('show');

  });

  $('#del_button').click(function(){

     $.ajax({
       url:"/deleteCommande/destroy/"+user_id,
       beforeSend:function(){
       $('#del_button').text('Suppression...');
       },
       success:function(data)
       {
       setTimeout(function(){
        $('#confirmModal').modal('hide');
       // $('#user_table').DataTable().ajax.reload();
       $('#commTable').DataTable().ajax.reload();

       $('#del_button').text('Confirmer');

        }, 1000);
       }
     })


     });

     $(document).on('click', '.confirm', function(){
        var id = $(this).attr('id');
        idu = $(this).attr('id');
        var idu = document.getElementById('iduser').value
        $('#user_id').val(idu);

        $('#form_result').html('');
        console.log(id)
        $.ajax({
         url :"/commande/"+id+"/edit",
         dataType:"json",
         success:function(data)
         {

         $('#nom_com').text(data.result.nom_com);
          $('#nom_client').text(data.result.nom_client);
          $('#lieu').text(data.result.lieu);
          $('#livraison').text(data.result.livraison);
          /* $('#prix_moyen').val(data.result.prix_moyen);
          $('.image').html("<img src={{ URL::to('/') }}/imagesCenter/" + data.result.photo + " width='70' class='img-thumbnail' />");
          $('.photo').html("<img src={{ URL::to('/') }}/imagesCenter/" + data.result.photo + " width='70' class='img-thumbnail' />");
          $('#action').val('Edit');
          $('#menu_button').html('Edit');
          $('#title').text('Modifier une catégorie');

          */
          $('#id').val(id);
          $('#ModalCommande').modal('show');

         }
        })

       });

       $('#form_commande').on('submit', function(event){
          event.preventDefault();
          var action_url = '';

          action_url = "{{ route('update.commande') }}";


          $.ajax({
           url: action_url,
           method:"POST",
           data:$(this).serialize(),
           dataType:"json",
           success:function(data)
           {
            var html = '';

            if(data.success)
            {

                html =
                '<div class="alert alert-success alert-dismissible" role="alert">'
                +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                  +'<span aria-hidden="true">&times;</span>'
                +'</button>'+data.success+'</div>';
           //  $('#user_table').DataTable().ajax.reload();
               //$('#nom_categorie').val('');
               $('#commTable').DataTable().ajax.reload();
            }
           $('#form_result').html(html);
           setTimeout(function(){

            $('#ModalCommande').modal('hide');
          }, 1500);
            //$('#ModalCategorie').modal('hide');
          },
           error:function(data){
             html =
             '<div class="alert alert-danger alert-dismissible" role="alert">'
             +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
               +'<span aria-hidden="true">&times;</span>'
             +'</button>'+'Echec de la confirmation'+'</div>';

             $('#form_result').html(html);

           },
          });
        });

        var id;
        $(document).on('click', '.commande', function(){
        id = $(this).attr('id');

        var id = document.getElementById('iduser').value
        console.log(id);
        $('#listecommTable').DataTable({

        //  $('#user_id').val(id);
            processing: true,
            serverSide: true,
            ajax: "/listeCommande/"+id,
            columns: [
                //{data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'nom_com', name: 'nom_com'},
                {data: 'nom_client', name: 'nom_client'},
                {data: 'phone', name: 'phone'},
                {data: 'email', name: 'email'},
                {data: 'livraison', name: 'livraison'},
                {data: 'lieu', name: 'lieu'},
              //  {data: 'status', name: 'status'},
                {data: 'updated_at', name: 'updated_at'},
            ]
        });

        $('#listeModal').modal('show');

        });



})
</script>


@endsection
