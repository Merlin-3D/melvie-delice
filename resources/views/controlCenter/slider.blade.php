@extends('layouts.contenu')

@section('title')
  Control Center - Sliders
@endsection

@section('content')

<div class="container-fluid" id="container-wrapper">
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-sliders-h"></i> Sliders</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
      <li class="breadcrumb-item">slider</li>
    </ol>
  </div>

  <div class="text-center">
    <div class="alert alert-light" role="alert">
      <span  class="btn btn-info btn-sm">
        <i class="fas fa-info-circle"></i>
      Ajouter && liste des Slides.</span>
    </div>


    <div class="row">
      <!-- Datatables -->
      <div class="col-lg-12">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6>
              <button type="button" class="btn btn-outline-danger mb-1 float-md-left new_slider" data-toggle="modal" data-target="#ModalSlider">
              <i class="fas fa-plus"></i> Ajouter un Slide</button>
            </h6>
          </div>

        </div>
      </div>
      <hr>
      <!-- DataTable with Hover -->
      <div class="col-lg-12">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Liste des slides</h6>
          </div>
          <div class="table-responsive p-3">
            <table class="table align-items-center table-flush table-hover" id="SliderTable">
              <thead class="thead-light">
                <tr>
                  <th>Titres</th>
                  <th>Sous Titres</th>
                  <th>photo</th>
                  <th>Date Creation</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>

                  <th>Titres</th>
                  <th>Sous Titres</th>
                  <th>photo</th>
                  <th>Date Creation</th>
                  <th>Action</th>
                </tr>
              </tfoot>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


  </div>


</div>

<div class="modal fade" id="ModalSlider" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-sliders-h"></i> Sliders</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <div  role="alert">
              <span  class="btn btn-info btn-sm" id="title">
                <i class="fas fa-info-circle"></i></span>
            </div>


          </div>
          <hr>
          <div class="card-body">
            <span id="form_result"></span>
            <form method="post"  id="form_slider" class="form-horizontal">
              @csrf
              <div class="form-group">
                <input type="hidden" class="form-control" name="user_id" id="user_id">

                <input type="text" class="form-control" name="titre" id="titre" aria-describedby="textlHelp"
                  placeholder="Entrer un titre">
              </div>
              <div class="form-group">

                <input type="text" class="form-control" name="sous_titre" id="sous_titre" aria-describedby="textlHelp"
                  placeholder="Entrer un sous-titre">
              </div>
              <div class="form-group">
                <div class="custom-file">
                  <input type="file" class="" name="photo" id="photo">
                  <label class="" for="customFile" name="image" id="image">Choisir une image</label>
                </div>
              </div>


          </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="action" id="action" value="Add" />
        <button type="button" class="btn btn-outline-danger mb-1 float-md-left" data-dismiss="modal">Close</button>
        <input type="hidden" name="id" id="id" />
        <button type="submit" id="slider_button" class="btn btn-outline-primary mb-1 float-md-left">Ajouter</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog"
  aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">

      <div class="modal-body">
        <div class="alert alert-danger alert-dismissible" role="alert">

          <h6><i class="fas fa-info"></i><b> Confirmation ...!</b></h6>
          Voulez-vous vraiment supprimer ce menu ??
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary mb-1 float-md-left" data-dismiss="modal">Non</button>

        <button type="button" name="del_button" id="del_button" class="btn btn-outline-danger mb-1 float-md-left">Confirmer</button>
      </div>
    </div>
  </div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">

$(document).ready(function(){

  $(function () {

    var id;
    var id = document.getElementById('iduser').value
    console.log(id);
    $('#SliderTable').DataTable({

        processing: true,
        serverSide: true,
        ajax: "/allSlider/"+id,
        columns: [


            {data: 'titre', name: 'titre'},
            {data: 'sous_titre', name: 'sous_titre'},
            {data: 'photo', name: 'photo', render: function(data, type, full, meta){
   return "<img src={{ URL::to('/') }}/imagesSlider/" + data + " width='70' class='img-thumbnail' />";
          },orderable: false},
            {data: 'updated_at', name: 'updated_at'},

            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

  });


  var id;
  $(document).on('click', '.new_slider', function(){

  id = $(this).attr('id');
  var id = document.getElementById('iduser').value
  $('#user_id').val(id);
  $('#title').text('Ajouter un Slider');
  $('#titre').val('');
  $('#sous_titre').val('');
  $('#image').text('Choisir une image');

  $('#slider_button').html('Ajouter')
  $('#ModalSlider').modal('show');

  });

  $('#form_slider').on('submit', function(event){
     event.preventDefault();
     var action_url = '';


      if($('#action').val() == 'Add')
      {
       action_url = "/storeSlider";

      }

      if($('#action').val() == 'Edit')
      {

       action_url = "{{ route('update.slider') }}";

      }

     $.ajax({
      url: action_url,
      method:"POST",
      data:new FormData(this),
      contentType:false,
      cache:false,
      processData:false,
      dataType:"",
      success:function(data)
      {

       var html = '';

       if(data.success)
       {

           html =
           '<div class="alert alert-success alert-dismissible" role="alert">'
           +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
             +'<span aria-hidden="true">&times;</span>'
           +'</button>'+data.success+'</div>';
      //  $('#user_table').DataTable().ajax.reload();
            $('#titre').val('');
            $('#sous_titre').val('');
            $('#photo').val('');

            $('#SliderTable').DataTable().ajax.reload();

          //$('.tableCategorie').DataTable().ajax.reload();
       }
      $('#form_result').html(html);
      setTimeout(function(){

       $('#ModalSlider').modal('hide');
     }, 1500);
       //$('#ModalCategorie').modal('hide');
     },
      error:function(data){
        html =
        '<div class="alert alert-danger alert-dismissible" role="alert">'
        +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
          +'<span aria-hidden="true">&times;</span>'
        +'</button>'+'Vérifier vos informations'+'</div>';

        $('#form_result').html(html);

      },
     });
   });

   var user_id;
   $(document).on('click', '.delete', function(){
   user_id = $(this).attr('id');

   $('#confirmModal').modal('show');

   });

   $('#del_button').click(function(){

      $.ajax({
        url:"/deleteSlider/destroy/"+user_id,
        beforeSend:function(){
        $('#del_button').text('Suppression...');
        },
        success:function(data)
        {
        setTimeout(function(){
         $('#confirmModal').modal('hide');
        // $('#user_table').DataTable().ajax.reload();
        $('#SliderTable').DataTable().ajax.reload();

        $('#del_button').text('Confirmer');

         }, 1000);
        }
      })


      });

      $(document).on('click', '.edit', function(){
         var id = $(this).attr('id');
         idu = $(this).attr('id');
         var idu = document.getElementById('iduser').value
         $('#user_id').val(idu);

         $('#form_result').html('');
         console.log(id)
         $.ajax({
          url :"/slider/"+id+"/edit",
          dataType:"json",
          success:function(data)
          {
           $('#user_id').val(data.result.user_id);
           //$('#categorie_id').val(data.result.categorie_id);
           $('#titre').val(data.result.titre);
           $('#sous_titre').val(data.result.sous_titre);
           $('#photo').html("<img src={{ URL::to('/') }}/imagesSlider/" + data.result.photo + " width='70' class='img-thumbnail' />");
           $('#image').html("<img src={{ URL::to('/') }}/imagesSlider/" + data.result.photo + " width='70' class='img-thumbnail' />");

           $('#id').val(id);
           $('#title').text('Modifier une Slider');
           $('#slider_button').html('Edit');
           $('#action').val('Edit');
           $('#ModalSlider').modal('show');
          }
         })

        });


})




</script>
@endsection
