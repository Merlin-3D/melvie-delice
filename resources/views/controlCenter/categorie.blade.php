@extends('layouts.contenu')

@section('title')
  Control Center - Categories
@endsection

@section('content')

  <div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-hamburger"></i> Catégories</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item">catégories</li>
      </ol>
    </div>

    <div class="text-center">
      <div class="alert alert-light" role="alert">
        <span  class="btn btn-info btn-sm">
          <i class="fas fa-info-circle"></i>
        Ajouter une Catégorie et consulter la liste des Catégories.</span>
      </div>


      <div class="row">
        <!-- Datatables -->
        <div class="col-lg-12">
          <div class="card mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6>  <button type="button" class="btn btn-outline-danger mb-1 float-md-left newcategorie">
                <i class="fas fa-plus"></i> Ajouter une categorie</button></h6>
            </div>

          </div>
        </div>
        <hr>
        <!-- DataTable with Hover -->
        <div class="col-lg-12">
          <div class="card mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Liste de Catégories</h6>
            </div>
            <div class="table-responsive p-3">
              <table class="table align-items-center table-flush table-hover tableCategorie">
                <thead class="thead-light">
                  <tr>
                    <th>Id</th>
                    <th>Nom</th>
                    <th>Date d'ajout</th>
                    <th>Mise à jour</th>
                    <th width="25%">Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Id</th>
                    <th>Nom</th>
                    <th>Date d'ajout</th>
                    <th>Mise à jour</th>
                    <th width="25%">Action</th>
                  </tr>
                </tfoot>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>


    </div>


  </div>

  <div class="modal fade" id="ModalCategorie" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-plus"></i> <span  class="title"></span></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="card mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              
              <div  role="alert">
                <span  class="btn btn-info btn-sm">
                  <i class="fas fa-info-circle"></i>
                  <span  class="title"></span></span>
              </div>


            </div>
            <hr>
            <div class="card-body">

              <span id="form_result"></span>

              <form method="post"  id="form_categorie" class="form-horizontal">
                @csrf
                <div class="form-group">
                  <input type="hidden" class="form-control" id="user_id" name="user_id">

                  <label for="exampleInputEmail1">Nom de la catégorie</label>
                  <input type="text" class="form-control" id="nom_categorie" name="nom_categorie" aria-describedby="textlHelp"
                    placeholder="Entrer une catégorie">
                  <small id="emailHelp" class="form-text text-muted">Ajouter & Modifier une catégorie adapté a votre restaurant.</small>
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="action" id="action" value="Add" />
          <input type="hidden" name="id" id="id" />
          <button type="button" class="btn btn-outline-danger mb-1 float-md-left" data-dismiss="modal">Close</button>
          <button type="sublit" name="button_categorie" id="button_categorie" class="btn btn-outline-primary mb-1 float-md-left">Modifier</button>
        </div>

        </form>
      </div>
    </div>
  </div>


  <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">

        <div class="modal-body">
          <div class="alert alert-danger alert-dismissible" role="alert">

            <h6><i class="fas fa-info"></i><b> Confirmation ...!</b></h6>
            Voulez-vous vraiment supprimer cette categorie ??
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-secondary mb-1 float-md-left" data-dismiss="modal">Non</button>

          <button type="button" name="del_button" id="del_button" class="btn btn-outline-danger mb-1 float-md-left">Confirmer</button>
        </div>
      </div>
    </div>
  </div>



@endsection


@section('scripts')

  <script type="text/javascript">
  $(document).ready(function(){


    //Donner du tableau

    $(function () {

      var id;
      var id = document.getElementById('iduser').value
      console.log(id);
      $('.tableCategorie').DataTable({

      //  $('#user_id').val(id);
          processing: true,
          serverSide: true,
          ajax: "/all/"+id,
          columns: [
              {data: 'DT_RowIndex', name: 'DT_RowIndex'},
              {data: 'nom_categorie', name: 'nom_categorie'},
            //  {data: 'email', name: 'email'},
              {data: 'created_at', name: 'created_at'},
              {data: 'updated_at', name: 'updated_at'},

              {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });

    });

    //Ajout d'une categorie
    var id;
    $(document).on('click', '.newcategorie', function(){
    id = $(this).attr('id');
    var id = document.getElementById('iduser').value
    $('#user_id').val(id);

    $('#nom_categorie').val('');
    $('#action').val('Add');
    $('.title').text('Ajouter une catégorie');
    $('#button_categorie').html('Ajouter');

    $('#ModalCategorie').modal('show');

    });

    $('#form_categorie').on('submit', function(event){
       event.preventDefault();
       var action_url = '';


        if($('#action').val() == 'Add')
        {
         action_url = "/storeCategorie";
        }

        if($('#action').val() == 'Edit')
        {
         action_url = "{{ route('update.categorie') }}";
        }

       $.ajax({
        url: action_url,
        method:"POST",
        data:$(this).serialize(),
        dataType:"json",
        success:function(data)
        {
         var html = '';

         if(data.success)
         {

             html =
             '<div class="alert alert-success alert-dismissible" role="alert">'
             +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
               +'<span aria-hidden="true">&times;</span>'
             +'</button>'+data.success+'</div>';
        //  $('#user_table').DataTable().ajax.reload();
            $('#nom_categorie').val('');
            $('.tableCategorie').DataTable().ajax.reload();
         }
        $('#form_result').html(html);
        setTimeout(function(){

         $('#ModalCategorie').modal('hide');
       }, 1500);
         //$('#ModalCategorie').modal('hide');
       },
        error:function(data){
          html =
          '<div class="alert alert-danger alert-dismissible" role="alert">'
          +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
            +'<span aria-hidden="true">&times;</span>'
          +'</button>'+'Renseigner la catégorie'+'</div>';

          $('#form_result').html(html);

        },
       });
     });

     var user_id;
     $(document).on('click', '.delete', function(){
     user_id = $(this).attr('id');

     $('#confirmModal').modal('show');

     });

//Supprimer une catégorie
  $('#del_button').click(function(){

     $.ajax({
       url:"deleteCategorie/destroy/"+user_id,
       beforeSend:function(){
       $('#del_button').text('Suppression...');
       },
       success:function(data)
       {
       setTimeout(function(){
        $('#confirmModal').modal('hide');
       // $('#user_table').DataTable().ajax.reload();
       $('.tableCategorie').DataTable().ajax.reload();
       $('#del_button').text('Confirmer');

        }, 1000);
       }
     })


     });

     //Update Categorie


    $(document).on('click', '.edit', function(){
       var id = $(this).attr('id');
       $('#form_result').html('');

       $.ajax({
        url :"/categorie/"+id+"/edit",
        dataType:"json",
        success:function(data)
        {
         $('#user_id').val(data.result.id);
         $('#nom_categorie').val(data.result.nom_categorie);
         $('#id').val(id);
         $('.title').text('Modifier une catégorie');
         $('#button_categorie').html('Edit');
         $('#action').val('Edit');
         $('#ModalCategorie').modal('show');
        }
       })

      });



    });
  </script>

@endsection
