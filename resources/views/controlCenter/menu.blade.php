@extends('layouts.contenu')

@section('title')
  Control Center - Menu
@endsection

@section('content')

<div class="container-fluid" id="container-wrapper">
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-clipboard-list"></i> Menu</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
      <li class="breadcrumb-item">menu</li>
    </ol>
  </div>

  <div class="text-center">
    <div class="alert alert-light" role="alert">
      <span  class="btn btn-info btn-sm">
        <i class="fas fa-info-circle"></i>
      Ajouter && liste des Menu.</span>
    </div>


    <div class="row">
      <!-- Datatables -->
      <div class="col-lg-12">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6>
              <button type="button" class="new_menu btn btn-outline-danger mb-1 float-md-left " data-toggle="modal" data-target="#ModalMenu">
              <i class="fas fa-plus"></i> Ajouter un Menu</button>
            </h6>
          </div>

        </div>
      </div>
      <hr>
      <!-- DataTable with Hover -->
      <div class="col-lg-12">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Liste des menus</h6>
          </div>
          <div class="table-responsive p-3">
            <table class="table align-items-center table-flush table-hover" id="ManuTable">
              <thead class="thead-light">
                <tr>
                  <th>Nom</th>
                  <th>Photo</th>
                  <th>categorie</th>
                  <th>Description</th>
                  <th>Prix</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Nom</th>
                  <th>Photo</th>
                  <th>categorie</th>
                  <th>Description</th>
                  <th>Prix</th>
                  <th>Action</th>
                </tr>
              </tfoot>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


  </div>


</div>

<div class="modal fade" id="ModalMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-clipboard-list"></i> Menu</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <div  role="alert">
              <span  class="btn btn-info btn-sm">
                <i class="fas fa-info-circle"></i>
                <span id="title"></span>
            </span>
            </div>


          </div>
          <hr>
          <div class="card-body">
            <span id="form_result"></span>

            <form method="post"  id="form_menu" class="form-horizontal">
              @csrf
              <select class="form-control mb-3" name="categorie_id" id="categorie_id">
                <option>Choisir la Catégorie</option>
                @foreach ($categories as $categorie )
                      <option value="{{$categorie->id}}">{{$categorie->nom_categorie}}</option>
                 @endforeach
              </select>
              <div class="form-group">
              <input type="hidden" class="form-control" name="user_id" id="user_id">

              <input type="text" class="form-control" name="nom_menu" id="nom_menu" aria-describedby="textlHelp"
                  placeholder="Entrer un nom">
              </div>
              <div class="form-group">
                <textarea class="form-control" id="description" name="description" rows="3" placeholder="Entrer une description"></textarea>


              </div>
              <div class="form-group">

                <input type="text" class="form-control" id="prix_moyen" name="prix_moyen" aria-describedby="textlHelp"
                  placeholder="Entrer ule prix">
              </div>

              <div class="form-group">
                <div class="">
                  <input type="file" class="photo" id="photo" name="photo">
                  <label class="image" for="customFile">Choisir une image</label>
                </div>
              </div>


          </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="action" id="action" value="Add" />
        <button type="button" class="btn btn-outline-danger mb-1 float-md-left" data-dismiss="modal">Close</button>
        <input type="hidden" name="id" id="id" />
        <button type="submit" id="menu_button" class="btn btn-outline-primary mb-1 float-md-left">Ajouter</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog"
  aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">

      <div class="modal-body">
        <div class="alert alert-danger alert-dismissible" role="alert">

          <h6><i class="fas fa-info"></i><b> Confirmation ...!</b></h6>
          Voulez-vous vraiment supprimer ce menu ??
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary mb-1 float-md-left" data-dismiss="modal">Non</button>

        <button type="button" name="del_button" id="del_button" class="btn btn-outline-danger mb-1 float-md-left">Confirmer</button>
      </div>
    </div>
  </div>
</div>
@endsection


@section('scripts')
  <script type="text/javascript">
  $(document).ready(function(){

    $(function () {

      var id;
      var id = document.getElementById('iduser').value
      console.log(id);
      $('#ManuTable').DataTable({

          processing: true,
          serverSide: true,
          ajax: "/allMenu/"+id,
          columns: [

              {data: 'nom_menu', name: 'nom_menu'},
              {data: 'photo', name: 'photo', render: function(data, type, full, meta){
     return "<img src={{ URL::to('/') }}/imagesCenter/" + data + " width='70' class='img-thumbnail' />";
            },orderable: false},
              {data: 'categorie_id', name: 'categorie_id'},
              {data: 'description', name: 'description'},
              {data: 'prix_moyen', name: 'prix_moyen'},

              {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });

    });


    //Ajouter un Menu

    var id;
    $(document).on('click', '.new_menu', function(){

    id = $(this).attr('id');
    var id = document.getElementById('iduser').value
    $('#user_id').val(id);
    $('#title').text('Ajouter un Menu');
    $('#nom_menu').val('');
    $('#description').val('');
    $('#prix_moyen').val('');
    $('.image').text('Choisir une image');

    $('#menu_button').html('Ajouter')
    $('#ModalMenu').modal('show');

    });

    $('#form_menu').on('submit', function(event){
       event.preventDefault();
       var action_url = '';


        if($('#action').val() == 'Add')
        {
         action_url = "/storeMenu";

        }

        if($('#action').val() == 'Edit')
        {

         action_url = "{{ route('update.menu') }}";

        }

       $.ajax({
        url: action_url,
        method:"POST",
        data:new FormData(this),
        contentType:false,
        cache:false,
        processData:false,
        dataType:"",
        success:function(data)
        {

         var html = '';

         if(data.success)
         {

             html =
             '<div class="alert alert-success alert-dismissible" role="alert">'
             +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
               +'<span aria-hidden="true">&times;</span>'
             +'</button>'+data.success+'</div>';
        //  $('#user_table').DataTable().ajax.reload();
              $('#nom_menu').val('');
              $('#description').val('');
              $('#prix_moyen').val('');
              $('#photo').val('');

              $('#ManuTable').DataTable().ajax.reload();

            //$('.tableCategorie').DataTable().ajax.reload();
         }
        $('#form_result').html(html);
        setTimeout(function(){

         $('#ModalMenu').modal('hide');
       }, 1500);
         //$('#ModalCategorie').modal('hide');
       },
        error:function(data){
          html =
          '<div class="alert alert-danger alert-dismissible" role="alert">'
          +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
            +'<span aria-hidden="true">&times;</span>'
          +'</button>'+'Vérifier vos informations'+'</div>';

          $('#form_result').html(html);

        },
       });
     });


//supprimer

  var user_id;
  $(document).on('click', '.delete', function(){
  user_id = $(this).attr('id');

  $('#confirmModal').modal('show');

  });

  $('#del_button').click(function(){

     $.ajax({
       url:"/deleteMenu/destroy/"+user_id,
       beforeSend:function(){
       $('#del_button').text('Suppression...');
       },
       success:function(data)
       {
       setTimeout(function(){
        $('#confirmModal').modal('hide');
       // $('#user_table').DataTable().ajax.reload();
       $('#ManuTable').DataTable().ajax.reload();

       $('#del_button').text('Confirmer');

        }, 1000);
       }
     })


     });

     $(document).on('click', '.edit', function(){
        var id = $(this).attr('id');
        idu = $(this).attr('id');
        var idu = document.getElementById('iduser').value
        $('#user_id').val(idu);

        $('#form_result').html('');
        console.log(id)
        $.ajax({
         url :"/menu/"+id+"/edit",
         dataType:"json",
         success:function(data)
         {
          $('#user_id').val(data.result.user_id);
          $('#categorie_id').val(data.result.categorie_id);
          $('#nom_menu').val(data.result.nom_menu);
          $('#description').val(data.result.description);
          $('#prix_moyen').val(data.result.prix_moyen);
          $('.image').html("<img src={{ URL::to('/') }}/imagesCenter/" + data.result.photo + " width='70' class='img-thumbnail' />");
          $('.photo').html("<img src={{ URL::to('/') }}/imagesCenter/" + data.result.photo + " width='70' class='img-thumbnail' />");

          $('#id').val(id);
          $('#title').text('Modifier une catégorie');
          $('#menu_button').html('Edit');
          $('#action').val('Edit');
          $('#ModalMenu').modal('show');
         }
        })

       });


  })

  </script>
@endsection
