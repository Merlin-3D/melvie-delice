@extends('layouts.contenu')

@section('title')
  Control Center - Profile
@endsection

@section('content')

<div class="container-fluid" id="container-wrapper">
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400">
    </i> Profile Du Restaurant</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
      <li class="breadcrumb-item">profile</li>
    </ol>
  </div>
<hr>

  <div class="container emp-profile">
        <span id="form_result"></span>
            <form method="post" id="form_photo">
                <div class="row">
                    <div class="col-md-4">
                        <div class="profile-img">
                            <img src="" alt="" value="{{ Auth::user()->photo }}"/>
                            <div class="file btn btn-lg btn-primary">
                                Change Photo
                                <input type="file" class="photo" id="photo" name="photo">
                            </div>
                            <input type="hidden" name="id" id="id" />
                            <input type="submit" class="profile-edit-btn editphoto" name="btnAddMore" value="Edit Photo"/>

                        </div>
                    </div>

              </form>


                    <div class="col-md-6">
                        <div class="profile-head">
                                    <h5>
                                        {{ Auth::user()->name_restaurant }}
                                    </h5>
                                    <h6>
                                        {{ Auth::user()->name }}
                                    </h6>
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">A propos</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Timeline</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <input type="submit" class="profile-edit-btn" name="btnAddMore" value="Edit Profile"/>
                    </div>
                </div>

                <form method="post">

                <div class="row">
                    <div class="col-md-4">
                        <div class="profile-work">
                            <p>INFO PERSO</p>
                            <a href="">
                              {{ Auth::user()->name }}
                            </a><br/>
                            <a href="">
                              {{ Auth::user()->phone }}
                            </a><br/>
                            <a href="">
                              {{ Auth::user()->email }}
                            </a><br/>
                            <p>INFO RESTAU</p>
                            <a href="">
                              {{ Auth::user()->name_restaurant }}
                            </a><br/>
                            <a href="">
                              {{ Auth::user()->lieu }}
                            </a><br/>
                            <a href="">
                              {{ Auth::user()->region }}
                            </a><br/>
                            <a href="">
                              {{ Auth::user()->prix_moyen }}
                            </a><br/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="tab-content profile-tab" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Nom :</label>
                                            </div>
                                            <div class="col-md-6">
                                              <p>
                                                {{ Auth::user()->name }}

                                              </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Télèphone :</label>
                                            </div>
                                            <div class="col-md-6">
                                              <p>
                                                {{ Auth::user()->phone }}

                                              </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Email</label>
                                            </div>
                                            <div class="col-md-6">
                                              <p>
                                                {{ Auth::user()->email }}

                                              </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Nom Restaurant :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p>
                                                  {{ Auth::user()->name_restaurant }}

                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Lieu</label>
                                            </div>
                                            <div class="col-md-6">
                                              <p>
                                                {{ Auth::user()->lieu }}

                                              </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Region</label>
                                            </div>
                                            <div class="col-md-6">
                                              <p>
                                                {{ Auth::user()->region }}

                                              </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Prix Moyen</label>
                                            </div>
                                            <div class="col-md-6">
                                              <p>
                                                {{ Auth::user()->prix_moyen }} XFA

                                              </p>
                                            </div>
                                        </div>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Experience</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p>Expert</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Hourly Rate</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p>10$/hr</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Total Projects</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p>230</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>English Level</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p>Expert</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Availability</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p>6 months</p>
                                            </div>
                                        </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Your Bio</label><br/>
                                        <p>Your detail description</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

</div>


@endsection


@section('scripts')
  <script type="text/javascript">


</script>
@endsection
