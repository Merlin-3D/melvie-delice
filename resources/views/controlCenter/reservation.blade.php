@extends('layouts.contenu')

@section('title')
  Control Center - Reservation
@endsection

@section('content')

<div class="container-fluid" id="container-wrapper">
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-edit"></i> Réservations</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
      <li class="breadcrumb-item">réservation</li>
    </ol>
  </div>

  <div class="text-center">
    <div class="alert alert-light" role="alert">
      <span  class="btn btn-info btn-sm">
        <i class="fas fa-info-circle"></i>
      Consulter la liste des Reservations.</span>
    </div>


    <div class="row">

      <!-- DataTable with Hover -->
      <div class="col-lg-12">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Liste de Réservations</h6>
          </div>
          <div class="table-responsive p-3">
            <table class="table align-items-center table-flush table-hover" id="reservTable">
              <thead class="thead-light">
                <tr>
                  <th>Nom</th>
                  <th>Télèphone</th>
                  <th>Email</th>
                  <th>Message</th>
                  <th>Status</th>
                  <th>Date d'envoi</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Nom</th>
                  <th>Télèphone</th>
                  <th>Email</th>
                  <th>Message</th>
                  <th>Status</th>
                  <th>Date d'envoi</th>
                  <th>Action</th>
                </tr>
              </tfoot>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


  </div>


</div>

<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog"
  aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">

      <div class="modal-body">
        <div class="alert alert-danger alert-dismissible" role="alert">

          <h6><i class="fas fa-info"></i><b> Confirmation ...!</b></h6>
          Voulez-vous vraiment supprimer cette reservation ??
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary mb-1 float-md-left" data-dismiss="modal">Non</button>

        <button type="button" name="del_button" id="del_button" class="btn btn-outline-danger mb-1 float-md-left">Confirmer</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="ModalReservation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-edit"></i> Reservation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <div  role="alert">
              <span  class="btn btn-info btn-sm">
                <i class="fas fa-info-circle"></i>
                <span id="title">Confirmer la Reservation</span>
            </span>
            </div>


          </div>
          <hr>
          <div class="card-body">

            <div class="form-group">
              <span id="rnom"></span>
            </div>
            <div class="form-group">
              <span id="rphone"></span>
            </div>
            <div class="form-group">
              <span id="remail"></span>
            </div>
            <hr>
            <div class="form-group">
              <span id="rmessage"></span>
            </div>
            <hr>

            <span id="form_result"></span>

            <form method="post"  id="form_reservation" class="form-horizontal">
              @csrf
              <select class="form-control mb-3" name="status" id="status">
                  <option value="1">Confirmer</option>
              </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="action" id="action" value="Add" />
        <button type="button" class="btn btn-outline-danger mb-1 float-md-left" data-dismiss="modal">Close</button>
        <input type="hidden" name="id" id="id" />
        <button type="submit" id="reservation_button" class="btn btn-outline-primary mb-1 float-md-left">Confirmer</button>
      </div>
      </form>
    </div>
  </div>
</div>

@endsection


@section('scripts')
  <script type="text/javascript">
  $(document).ready(function(){

  $(function () {

    var id;
    var id = document.getElementById('iduser').value
    console.log(id);
    $('#reservTable').DataTable({

    //  $('#user_id').val(id);
        processing: true,
        serverSide: true,
        ajax: "/allReservation/"+id,
        columns: [
            //{data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'nom', name: 'nom'},
            {data: 'phone', name: 'phone'},
            {data: 'email', name: 'email'},
            {data: 'message', name: 'message'},
            {data: 'status', name: 'status'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

  });

  var user_id;
  $(document).on('click', '.delete', function(){
  user_id = $(this).attr('id');

  $('#confirmModal').modal('show');

  });

  $('#del_button').click(function(){

     $.ajax({
       url:"/deleteReservation/destroy/"+user_id,
       beforeSend:function(){
       $('#del_button').text('Suppression...');
       },
       success:function(data)
       {
       setTimeout(function(){
        $('#confirmModal').modal('hide');
       // $('#user_table').DataTable().ajax.reload();
       $('#reservTable').DataTable().ajax.reload();

       $('#del_button').text('Confirmer');

        }, 1000);
       }
     })


     });

     $(document).on('click', '.confirm', function(){
        var id = $(this).attr('id');
        idu = $(this).attr('id');
        var idu = document.getElementById('iduser').value
        $('#user_id').val(idu);

        $('#form_result').html('');
        console.log(id)
        $.ajax({
         url :"/reservation/"+id+"/edit",
         dataType:"json",
         success:function(data)
         {
           console.log(data.result.status)
           console.log(data.result.nom)
           console.log(data.result.phone)
           console.log(data.result.email)
           console.log(data.result.message)

         $('#rnom').text(data.result.nom);
          $('#rphone').text(data.result.phone);
          $('#remail').text(data.result.email);
          $('#rmessage').text(data.result.message);
          /* $('#prix_moyen').val(data.result.prix_moyen);
          $('.image').html("<img src={{ URL::to('/') }}/imagesCenter/" + data.result.photo + " width='70' class='img-thumbnail' />");
          $('.photo').html("<img src={{ URL::to('/') }}/imagesCenter/" + data.result.photo + " width='70' class='img-thumbnail' />");
          $('#action').val('Edit');
          $('#menu_button').html('Edit');
          $('#title').text('Modifier une catégorie');

          */
          $('#id').val(id);
          $('#ModalReservation').modal('show');

         }
        })

       });

       $('#form_reservation').on('submit', function(event){
          event.preventDefault();
          var action_url = '';

          action_url = "{{ route('update.reservation') }}";


          $.ajax({
           url: action_url,
           method:"POST",
           data:$(this).serialize(),
           dataType:"json",
           success:function(data)
           {
            var html = '';

            if(data.success)
            {

                html =
                '<div class="alert alert-success alert-dismissible" role="alert">'
                +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                  +'<span aria-hidden="true">&times;</span>'
                +'</button>'+data.success+'</div>';
           //  $('#user_table').DataTable().ajax.reload();
               //$('#nom_categorie').val('');
               $('#reservTable').DataTable().ajax.reload();
            }
           $('#form_result').html(html);
           setTimeout(function(){

            $('#ModalReservation').modal('hide');
          }, 1500);
            //$('#ModalCategorie').modal('hide');
          },
           error:function(data){
             html =
             '<div class="alert alert-danger alert-dismissible" role="alert">'
             +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
               +'<span aria-hidden="true">&times;</span>'
             +'</button>'+'Echec de la confirmation'+'</div>';

             $('#form_result').html(html);

           },
          });
        });



})
</script>
@endsection
