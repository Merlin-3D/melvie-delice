@extends('layouts.contenu')

@section('title')
  Control Center - Contact
@endsection

@section('content')

<div class="container-fluid" id="container-wrapper">
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-comments"></i> Contact</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
      <li class="breadcrumb-item">contact</li>
    </ol>
  </div>

  <div class="text-center">
    <div class="alert alert-light" role="alert">
      <span  class="btn btn-info btn-sm">
        <i class="fas fa-info-circle"></i>
      Consulter la liste des Messages reçu.</span>
    </div>


    <div class="row">

      <!-- DataTable with Hover -->
      <div class="col-lg-12">
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Liste des Contacts</h6>
          </div>
          <div class="table-responsive p-3">
            <table class="table align-items-center table-flush table-hover" id="contactTable">
              <thead class="thead-light">
                <tr>
                  <th>Id</th>
                  <th>Nom</th>
                  <th>Email</th>
                  <th>Sujet</th>
                  <th>Date d'envoi</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Nom</th>
                  <th>Email</th>
                  <th>Sujet</th>
                  <th>Date d'envoi</th>
                  <th>Action</th>
                </tr>
              </tfoot>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


  </div>


</div>


<div class="modal fade" id="ModalContact" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-comments"></i> Contact</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card mb-4">
          <div class="alert alert-warning" role="alert">
          <i class="fas fa-info"></i>  Voir Le Message
          </div>

        <hr>

          <div class="card-body">

            <div class="form-group">
              <small>Nom : </small>
              <h6 id="rnom"></h6>
            </div>
            <div class="form-group">
              <small>Email : </small>
              <h6 id="remail"></h6>
            </div>
            <div class="form-group">
              <small>Date D'envoi : </small>
              <h6 id="rdate"></h6>
            </div>
            <hr>
            <small id="emailHelp" class="form-text text-muted">Message.</small>

            <div class="form-group">
              <h4 id="rsujet"></h4>
            </div>
            <hr>

            <span id="form_result"></span>

            <form method="post"  id="form_contact" class="form-horizontal">

          </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="action" id="action" value="Add" />
        <button type="button" class="btn btn-outline-danger mb-1 float-md-left" data-dismiss="modal">Close</button>
        <input type="hidden" name="id" id="id" />
      </div>
      </form>
    </div>
  </div>
</div>



<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog"
  aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">

      <div class="modal-body">
        <div class="alert alert-danger alert-dismissible" role="alert">

          <h6><i class="fas fa-info"></i><b> Confirmation ...!</b></h6>
          Voulez-vous vraiment supprimer ce Contact ??
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary mb-1 float-md-left" data-dismiss="modal">Non</button>

        <button type="button" name="del_button" id="del_button" class="btn btn-outline-danger mb-1 float-md-left">Confirmer</button>
      </div>
    </div>
  </div>
</div>
@endsection


@section('scripts')
  <script type="text/javascript">
  $(document).ready(function(){

  $(function () {

    var id;
    var id = document.getElementById('iduser').value
    console.log(id);
    $('#contactTable').DataTable({

    //  $('#user_id').val(id);
        processing: true,
        serverSide: true,
        ajax: "/allContact/"+id,
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'nom', name: 'nom'},
            {data: 'email', name: 'email'},
            {data: 'sujet', name: 'sujet'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

  });

  var user_id;
  $(document).on('click', '.delete', function(){
  user_id = $(this).attr('id');

  $('#confirmModal').modal('show');

  });

  $('#del_button').click(function(){

     $.ajax({
       url:"/deleteContact/destroy/"+user_id,
       beforeSend:function(){
       $('#del_button').text('Suppression...');
       },
       success:function(data)
       {
       setTimeout(function(){
        $('#confirmModal').modal('hide');
       // $('#user_table').DataTable().ajax.reload();
       $('#contactTable').DataTable().ajax.reload();

       $('#del_button').text('Confirmer');

        }, 1000);
       }
     })


     });

     $(document).on('click', '.look', function(){
        var id = $(this).attr('id');
        idu = $(this).attr('id');
        var idu = document.getElementById('iduser').value
        $('#user_id').val(idu);

        $('#form_result').html('');
        console.log(id)
        $.ajax({
         url :"/contact/"+id+"/edit",
         dataType:"json",
         success:function(data)
         {
           console.log(data.result.nom)
           console.log(data.result.email)
           console.log(data.result.sujet)
           console.log(data.result.created_at)


         $('#rnom').text(data.result.nom);
          $('#remail').text(data.result.email);
          $('#rdate').text(data.result.created_at);
          $('#rsujet').text(data.result.sujet);
          /*$('#prix_moyen').val(data.result.prix_moyen);
          $('.image').html("<img src={{ URL::to('/') }}/imagesCenter/" + data.result.photo + " width='70' class='img-thumbnail' />");
          $('.photo').html("<img src={{ URL::to('/') }}/imagesCenter/" + data.result.photo + " width='70' class='img-thumbnail' />");
          $('#action').val('Edit');
          $('#menu_button').html('Edit');
          $('#title').text('Modifier une catégorie');

          */
          $('#id').val(id);
          $('#ModalContact').modal('show');

         }
        })

       });





})
</script>
@endsection
