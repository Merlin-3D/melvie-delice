@extends('layouts.app')

@section('content')
<div class="container">


  <div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
      <!-- Nested Row within Card Body -->
      <div class="row">
        <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
        <div class="col-lg-7">
          <div class="p-5">
            <div class="text-center">
              <h1 class="h4 text-gray-900 mb-4">
                <i class="fas fa-user-circle"></i> {{ __('Register') }} / {{ __('S\'enregistrer') }}</h1>
              <hr>
              <h6 class="h6 text-gray-900 mb-4">Informations Personnelles</h6>

            </div>
            <form class="user" method="POST" action="{{ route('register') }}">
              @csrf

              <div class="form-group">
                <input id="name" type="text" class="form-control form-control-user @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Votre Nom Complet" required autocomplete="name" autofocus>

                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
              <div class="form-group row">
                <div class="col-sm-6 mb-3 mb-sm-0">
                  <input id="phone" type="text" class="form-control form-control-user @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" placeholder="Telephone" required autocomplete="phone" autofocus>

                  @error('phone')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>
                <div class="col-sm-6">
                  <input id="email" type="email" class="form-control form-control-user @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email" required autocomplete="email">

                  @error('email')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-6 mb-3 mb-sm-0">
                  <input id="password" type="password" class="form-control form-control-user @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="new-password">

                  @error('password')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>
                <div class="col-sm-6">
                  <input id="password-confirm" type="password" class="form-control form-control-user" name="password_confirmation" placeholder="Confirm Password" required autocomplete="new-password">

                </div>
              </div>
              <hr>
              <h6 class="h6 text-gray-900 mb-4 text-center">Informations Du Restaurant</h6>
              <hr>
              <div class="form-group row">
                <div class="col-sm-6 mb-3 mb-sm-0">

                    <input id="name_restaurant" type="text" class="form-control form-control-user @error('name_restaurant') is-invalid @enderror" name="name_restaurant" value="{{ old('name_restaurant') }}" placeholder="Nom Restaurant" required autocomplete="name_restaurant" autofocus>

                    @error('name_restaurant')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-sm-6">
                  <input id="prix_moyen" type="text" class="form-control form-control-user @error('prix_moyen') is-invalid @enderror" name="prix_moyen" value="{{ old('prix_moyen') }}" placeholder="Prix Moyen" required autocomplete="prix_moyen" autofocus>

                  @error('prix_moyen')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-6 mb-3 mb-sm-0">

                    <input id="lieu" type="text" class="form-control form-control-user @error('lieu') is-invalid @enderror" name="lieu" value="{{ old('lieu') }}" placeholder="Lieu" required autocomplete="lieu" autofocus>

                    @error('lieu')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-sm-6">
                  <input id="region" type="text" class="form-control form-control-user @error('region') is-invalid @enderror" name="region" value="{{ old('region') }}" placeholder="Region" required autocomplete="region" autofocus>

                  @error('region')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>
                <input id="usertype" value="admin" type="hidden" class="form-control @error('usertype') is-invalid @enderror" name="usertype" value="{{ old('usertype') }}" required autocomplete="usertype" autofocus>

              </div>

              <button type="submit" class="btn btn-primary btn-block">
                  {{ __('S\'enregistrer') }}
              </button>



            </form>
            <hr>

          </div>
        </div>
      </div>


    </div>
  </div>

</div>
@endsection
