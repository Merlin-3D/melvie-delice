@extends('layouts.principale')

@section('title')
Login - Melvie Délice
@endsection


@section('content')

	<section class="banner-area organic-breadcrumb">
	  <div class="container">
	    <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
	      <div class="col-first">
	        <h1 style="color:#ff6c00">Connexion/Inscription</h1>

	      </div>
	    </div>
	  </div>
	</section>
	<!-- End Banner Area -->

	<!--================Login Box Area =================-->
	<section class="login_box_area section_gap">
	  <div class="container">
	    <div class="row">
	      <div class="col-lg-6">
	        <div class="login_box_img">
	          <img class="img-fluid" src="img/res02.jpg" alt="">
	          <div class="hover">

	            <p>Découvré toutes les merveilles que propose Melvie Délice </p>
	            <a class="primary-btn" href="">Crée un Compte</a>
	          </div>
	        </div>
	      </div>
	      <div class="col-lg-6">
	        <div class="login_form_inner">
	          <h3>Connexion</h3>
	          <form class="row login_form" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
	            <div class="col-md-12 form-group">
	              <input type="text" class="form-control" id="name" name="name" placeholder="Nom" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Username'">
	            </div>
	            <div class="col-md-12 form-group">
	              <input type="email" class="form-control" id="email" name="email" placeholder="exemple@gmail.com" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Username'">
	            </div>
	            <div class="col-md-12 form-group">
	              <input type="text" class="form-control" id="name" name="name" placeholder="Mot de Passe" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'">
	            </div>
	            <div class="col-md-12 form-group">
	              <div class="creat_account">
	                <input type="checkbox" id="f-option2" name="selector">
	                <label for="f-option2">Souvenir de moi</label>
	              </div>
	            </div>
	            <div class="col-md-12 form-group">
	              <button type="submit" value="submit" class="primary-btn">Connexion</button>
	              <a href="#">Mote de Passe Oublier?</a>
	            </div>
	          </form>
	        </div>
	      </div>
	    </div>
	  </div>
	</section>

@endsection
