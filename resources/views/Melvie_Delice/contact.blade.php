@extends('layouts.principale')

@section('title')
Contact - Melvie Délice
@endsection


@section('content')
	<section class="banner-area organic-breadcrumb">
	  <div class="container">
	    <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
	      <div class="col-first">
	        <h1 style="color:#ff6c00">Nous Contactés</h1>
	      </div>
	    </div>
	  </div>
	</section>
	<!-- End Banner Area -->

	<!--================Contact Area =================-->
	<section class="contact_area section_gap_bottom" style="margin-top: 15px;">
	  <div class="container">

	    <div class="row">
	      <div class="col-lg-3">
	        <div class="contact_info">
	          <div class="info_item">
	            <i class="lnr lnr-home"></i>
	            <h6>Douala, Cameroun</h6>
	            <p>Pk16-Douala</p>
	          </div>
	          <div class="info_item">
	            <i class="lnr lnr-phone-handset"></i>
	            <h6><a href="#">(+237) 655413390</a></h6>
	            <p>Disponible de 7h à 20h</p>
	          </div>
	          <div class="info_item">
	            <i class="lnr lnr-envelope"></i>
	            <h6><a href="#">melvie_delice@gmail.com</a></h6>
	            <p>Votre message a cette adresse!</p>
	          </div>
	        </div>
	      </div>
	      <div class="col-lg-9">
	        <form class="row contact_form" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
	          <div class="col-md-6">
	            <div class="form-group">
	              <input type="text" class="form-control" id="name" name="name" placeholder="Votre nom" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'">
	            </div>
	            <div class="form-group">
	              <input type="email" class="form-control" id="email" name="email" placeholder="Votre adresse mail" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'">
	            </div>
	            <div class="form-group">
	              <input type="text" class="form-control" id="subject" name="subject" placeholder="Entrer le sujet" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Subject'">
	            </div>
	          </div>
	          <div class="col-md-6">
	            <div class="form-group">
	              <textarea class="form-control" name="message" id="message" rows="1" placeholder="Entrer le Message" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Message'"></textarea>
	            </div>
	          </div>
	          <div class="col-md-12 text-right">
	            <button type="submit" value="submit" class="primary-btn">Envoyer</button>
	          </div>
	        </form>
	      </div>
	    </div>
	  </div>
	</section>

@endsection
