@extends('layouts.principale')

@section('title')
Melvie Délice
@endsection


@section('content')

	<section class="banner-area">
	  <div class="container">
	    <div class="row fullscreen align-items-center justify-content-start">
	      <div class="col-lg-12">
	        <div class="">
	          <!-- single-slide -->

	          <!-- single-slide -->
	          <div class="">

	            <div class="col-lg-7">
	              <div class="banner-img">
	                <img class="img-fluid" src="img/banner/banner-img.png" alt="">
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	      <div class=" align-items-center d-flex">
	        <div class="col-lg-12 col-md-7" style="margin-top:50px;">
	          <div class="banner-content">
	            <h2 style="color:#ff6c00;font-size:38px;">Découvrez et reservez <br>Dans les meilleurs restaurants</h2>

	          </div>
	        </div>
	        <div class="col-lg-7">
	          <div class="banner-img">
	            <img class="img-fluid" src="" alt="">
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>

	</section>

	<!-- End banner Area -->

	<!-- start features Area -->


	<div id="carouselExampleIndicators" class="carousel slide container" data-ride="carousel" style="margin-top:30px;">
	<ol class="carousel-indicators">
	  <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>
	  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
	  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
	</ol>
	<hr>
	<div>
	  <div class="col-lg-6 text-left">
	    <div class="">
	      <h4>Choisir votre restaurant</h4>
	    </div>
	  </div>
	</div>

	<div class="carousel-inner">

	  <div class="carousel-item active row">

	    <div class="row">
				@foreach($users as $user)
	      <div class="col-12 col-md-3">
	    <div class="card">
	      <div class="card-body">
	        <div class="col-lg-12">
	          <div class="banner-img">
	            <img class="img-fluid" src="img/insta1.jpg" alt="">
	          </div>
	        </div>
	        <div class="card-text">
	          <h5>{{ $user->name_restaurant}}</h5>
	          <span>{{ $user->lieu}}</span><br>
	          <sapn>Prix moyens : {{ $user->prix_moyen}} xfa</span><br>
	        </div>
	      </div>
	    </div>
	      </div>
			@endforeach



	    </div>
	  </div>
	  <div class="carousel-item">
	    <div class="row">
	      <div class="col-12 col-md-3">
	    <div class="card">
	      <div class="card-body">
	        <div class="col-lg-12">
	          <div class="banner-img">
	            <img class="img-fluid" src="img/insta4.jpg" alt="">
	          </div>
	        </div>
	        <div class="card-text">
	          <h5>Restau 4</h5>
	          <span>Congolais</span><br>
	          <span>lieu</span><br>
	          <sapn>Prix moyens : 2500 xfa</span><br>
	          <span>99 avis</span>
	        </div>
	      </div>
	    </div>
	      </div>

	      <div class="col-md-3 d-md-block d-none">
	      <div class="card">
	        <div class="card-body">
	          <div class="col-lg-12">
	            <div class="banner-img">
	              <img class="img-fluid" src="img/insta3.jpg" alt="">
	            </div>
	          </div>
	          <div class="card-text">
	            <h5>Restau 5</h5>
	            <span>Senegalais</span><br>
	            <span>lieu</span><br>
	            <sapn>Prix moyens : 2500 xfa</span><br>
	            <span>99 avis</span>
	          </div>
	        </div>
	      </div>
	        </div>

	        <div class="col-md-3 d-md-block d-none">
	        <div class="card">
	          <div class="card-body">
	            <div class="col-lg-12">
	              <div class="banner-img">
	                <img class="img-fluid" src="img/insta2.jpg" alt="">
	              </div>
	            </div>
	            <div class="card-text">
	              <h5>Restau 5</h5>
	              <span>Senegalais</span><br>
	              <span>lieu</span><br>
	              <sapn>Prix moyens : 2500 xfa</span><br>
	              <span>99 avis</span>
	            </div>
	          </div>
	        </div>
	          </div>

	          <div class="col-md-3 d-md-block d-none">
	          <div class="card">
	            <div class="card-body">
	              <div class="col-lg-12">
	                <div class="banner-img">
	                  <img class="img-fluid" src="img/insta1.jpg" alt="">
	                </div>
	              </div>
	              <div class="card-text">
	                <h5>Restau 5</h5>
	                <span>Senegalais</span><br>
	                <span>lieu</span><br>
	                <sapn>Prix moyens : 2500 xfa</span><br>
	                <span>99 avis</span>
	              </div>
	            </div>
	          </div>
	            </div>

	    </div>
	  </div>
	  <div class="carousel-item">
	    <div class="row">
	    <div class="col-md-3 d-md-block d-none">
	    <div class="card">
	      <div class="card-body">
	        <div class="col-lg-12">
	          <div class="banner-img">
	            <img class="img-fluid" src="img/insta1.jpg" alt="">
	          </div>
	        </div>
	        <div class="card-text">
	          <h5>Restau 5</h5>
	          <span>Senegalais</span><br>
	          <span>lieu</span><br>
	          <sapn>Prix moyens : 2500 xfa</span><br>
	          <span>99 avis</span>
	        </div>
	      </div>
	    </div>
	      </div>
	      <div class="col-md-3 d-md-block d-none">
	      <div class="card">
	        <div class="card-body">
	          <div class="col-lg-12">
	            <div class="banner-img">
	              <img class="img-fluid" src="img/insta2.jpg" alt="">
	            </div>
	          </div>
	          <div class="card-text">
	            <h5>Restau 5</h5>
	            <span>Senegalais</span><br>
	            <span>lieu</span><br>
	            <sapn>Prix moyens : 2500 xfa</span><br>
	            <span>99 avis</span>
	          </div>
	        </div>
	      </div>
	    </div>
	    <div class="col-md-3 d-md-block d-none">
	    <div class="card">
	      <div class="card-body">
	        <div class="col-lg-12">
	          <div class="banner-img">
	            <img class="img-fluid" src="img/insta3.jpg" alt="">
	          </div>
	        </div>
	        <div class="card-text">
	          <h5>Restau 5</h5>
	          <span>Senegalais</span><br>
	          <span>lieu</span><br>
	          <sapn>Prix moyens : 2500 xfa</span><br>
	          <span>99 avis</span>
	        </div>
	      </div>
	    </div>
	      </div>
	      <div class="col-md-3 d-md-block d-none">
	      <div class="card">
	        <div class="card-body">
	          <div class="col-lg-12">
	            <div class="banner-img">
	              <img class="img-fluid" src="img/insta4.jpg" alt="">
	            </div>
	          </div>
	          <div class="card-text">
	            <h5>Restau 5</h5>
	            <span>Senegalais</span><br>
	            <span>lieu</span><br>
	            <sapn>Prix moyens : 2500 xfa</span><br>
	            <span>99 avis</span>
	          </div>
	        </div>
	      </div>
	        </div>
	    </div>
	  </div>
	</div>
	<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
	  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	  <span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
	  <span class="carousel-control-next-icon" aria-hidden="true"></span>
	  <span class="sr-only">Next</span>
	</a>
	</div>




	<section class="features-area section_gap">
	  <div class="container">
	    <div>
	      <div class="col-lg-6 text-left">
	        <div class="">
	          <h4>Comment ça marche ?</h4>
	        </div>
	      </div>
	    </div>
	    <div class="row features-inner">
	      <!-- single features -->
	      <div class="col-lg-3 col-md-6 col-sm-6">
	        <div class="single-features">

	          <div class="f-icon">
	            <h2><i class="fa fa-handshake-o" aria-hidden="true"></i></h2>
	          </div>
	          <h6>meilleurs choix</h6>
	          <p>Une sélection inégalée de restaurants <br> pour tout ce que vous voulez.</p>
	        </div>
	      </div>
	      <!-- single features -->
	      <div class="col-lg-3 col-md-6 col-sm-6">
	        <div class="single-features">
	          <div class="f-icon">
	            <h2>
	              <i class="fa fa-star" aria-hidden="true"></i>
	              <i class="fa fa-star" aria-hidden="true"></i>
	              <i class="fa fa-star" aria-hidden="true"></i>
	              <i class="fa fa-star-half-o" aria-hidden="true"></i>
	            </h2>
	          </div>
	          <h6>Critiques d'utilisateurs</h6>
	          <p>Recommandations et avis <br> d'une puissante communauté.</p>
	        </div>
	      </div>
	      <!-- single features -->
	      <div class="col-lg-3 col-md-6 col-sm-6">
	        <div class="single-features">
	          <div class="f-icon">
	            <h2><i class="fa fa-truck" aria-hidden="true"></i></h2>
	          </div>
	          <h6>Livraison rapide</h6>
	          <p>De nombreux restaurants <br> un moyen de livraison rapide et éfficace</p>
	        </div>
	      </div>
	      <!-- single features -->
	      <div class="col-lg-3 col-md-6 col-sm-6">
	        <div class="single-features">
	          <div class="f-icon">
	            <h2><i class="fa fa-cutlery" aria-hidden="true"></i></h2>
	          </div>
	          <h6>Réservation facile</h6>
	          <p>Instantané, gratuit, partout. 24/7</p>
	        </div>
	      </div>
	    </div>
	  </div>
	</section>


	<section class="related-product-area section_gap_bottom">
	  <div class="container">
	    <div class="row justify-content-center">
	      <div class="col-lg-12 text-center">
	        <div class="section-title">
	          <h1>À propos de Melvie Délice</h1>
	          <p>Melvie Délice est une plateforme de réservation et de découverte en ligne au cameroun.
	            Réservez votre table dans les meilleurs restaurants de Douala ,
	            Yaoundé, Bafoussam, et plus encore. Recherchez la disponibilité du restaurant à tout moment
	             et au meilleur prix. Avec plus d'avis vérifiés pour vous guider, vous pouvez découvrir
	             le restaurant parfait sur Merlvie Délice.</p>
	        </div>
	      </div>
	    </div>
	  </div>
	</section>


@endsection
