<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title> @yield('title') </title>
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="../css/ruang-admin.min.css" rel="stylesheet">
  <link href="../css/style.css" rel="stylesheet">
</head>

<body id="page-top">
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/dashboard">
        <div class="sidebar-brand-icon">
          <img src="../img/logo/logo2.png">
        </div>
        <div class="sidebar-brand-text mx-3">{{ Auth::user()->name_restaurant }} </div>
      </a>
      <hr class="sidebar-divider my-0">
      <li class="nav-item active">
        <a class="nav-link" href="/dashboard">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>
      <hr class="sidebar-divider">

      <li class="nav-item">
        <a class="nav-link" id="liencategorie" href="">
          <i class="fas fa-hamburger"></i>
          <span>Categories</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link menu" id="lienmenu" href="">
          <i class="fas fa-clipboard-list"></i>
          <span>Menu</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" id="lienslider" href="">
          <i class="fas fa-sliders-h"></i>
          <span>Sliders</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" id="lienreservation"  href="">
          <i class="fas fa-edit"></i>
          <span>Reservation</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" id="liencommande" href="">
          <i class="fas fa-people-carry"></i>
          <span>Commande</span>
        </a>
      </li>


      <li class="nav-item">
        <a class="nav-link" id="liencontact" href="">
          <i class="fas fa-comments"></i>
          <span>Contact</span>
        </a>
      </li>


      <hr class="sidebar-divider">

    </ul>
    <!-- Sidebar -->
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <!-- TopBar -->
        <nav class="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top">
          <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>
          <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown no-arrow">

              <li class="nav-item dropdown no-arrow">
                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <img class="img-profile rounded-circle mr-2" src="../img/boy.png" style="max-width: 60px">
                      {{ Auth::user()->name }} <span class="caret"></span>
                  </a>
                  <input id="iduser" name="iduser" value="{{ Auth::user()->id }}" type="hidden" class="form-control" required  autofocus>


                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                      <a class="dropdown-item"  id="lienprofile" href="">
                        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                        Profile
                      </a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="{{ route('logout') }}"
                         onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                          <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                          {{ __('Logout') }}
                      </a>

                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                  </div>
              </li>

            </li>


          </ul>
        </nav>
        <!-- Topbar -->

          @yield('content')


        <!-- Container Fluid-->

      <!-- Footer -->

    </div>
    <footer class="sticky-footer bg-white">
      <div class="container my-auto">
        <div class="copyright text-center my-auto">
          <span>copyright &copy; 2019 - developed by
            <b><a href="" target="_blank">Melvie</a></b>
          </span>
        </div>
      </div>
    </footer>
    <!-- Footer -->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>
  </div>

  <!-- Scroll to top -->


  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="../js/ruang-admin.min.js"></script>
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <script src="../js/monjs.js"></script>
  <script>
    $(document).ready(function () {
      $('#dataTable').DataTable(); // ID From dataTable
      $('#dataTableHover').DataTable();
      //$('.tableCategorie').DataTable(); // ID From dataTable with Hover
      var id;
      //id = $(this).attr('id');
      var id = document.getElementById('iduser').value
      //$('#user_id').val(id);
      //console.log('dfd'+id)


        $('a#lienmenu').attr('href','/menu/'+id);
        $('a#liencategorie').attr('href','/categorie');
        $('a#lienslider').attr('href','/slider');
        $('a#lienreservation').attr('href','/reservation');
        $('a#liencommande').attr('href','/commande');
        $('a#liencontact').attr('href','/contact');
        $('a#lienprofile').attr('href','/profile');


    });
  </script>

  @yield('scripts')
</body>

</html>
