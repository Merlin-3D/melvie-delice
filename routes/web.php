<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route Principale
Route::get('/','principaleController@index');
//End Route Principale

//Route Affiche Principale
Route::view('/login_melvie','Melvie_Delice.login');
Route::view('/contact_melvie','Melvie_Delice.contact');
//End Route Affiche Principale


//Authentification
//Route::view('/','auth.login');
Route::view('/login','auth.login');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth','admin']],function(){

    Route::view('/dashboard','controlCenter.dashboard');

});
//end Authentification

//Route Profile
Route::post('profileImg/update', 'profileController@updatePhoto')->name('update.photo');

//End Route Profile

//Route Dashboard
Route::view('/categorie','controlCenter.categorie');
Route::view('/commande','controlCenter.commande');
Route::view('/profile','controlCenter.profile');
Route::view('/contact','controlCenter.contact');
Route::get('/menu/{id}','menuController@menu');
Route::view('/reservation','controlCenter.reservation');
Route::view('/slider','controlCenter.slider');
//End Route Dashboard

//Route Categorie
Route::post('/storeCategorie','categorieCotroller@storeCategorie');
Route::get('/all/{id}','categorieCotroller@allCategorie')/*->name('all.categorie')*/;
Route::get('deleteCategorie/destroy/{id}', 'categorieCotroller@destroyCategorie');
Route::post('categorie/update', 'categorieCotroller@updateCategorie')->name('update.categorie');
Route::get('/categorie/{id}/edit', 'categorieCotroller@edit');
//End Route Categorie


//Route Menu
Route::post('/storeMenu','menuController@storeMenu')/*->name('store.menu')*/;
Route::post('menu/update', 'menuController@updateMenu')->name('update.menu');
Route::get('/allMenu/{id}','menuController@allMenu');
Route::get('/deleteMenu/destroy/{id}', 'menuController@destroyMenu');
Route::get('/menu/{id}/edit', 'menuController@edit');
//End Route Menu

//Route Slider
Route::post('/storeSlider','sliderController@storeSlider');
Route::post('slider/update', 'sliderController@updateSlider')->name('update.slider');
Route::get('/allSlider/{id}','sliderController@allSlider');
Route::get('/deleteSl text-gray-400ider/destroy/{id}', 'sliderController@destroySlider');
Route::get('/slider/{id}/edit', 'sliderController@edit');
//End Route Slider

//Route Reservation
Route::get('/allReservation/{id}','reservationController@allReservation');
Route::get('/deleteReservation/destroy/{id}', 'reservationController@destroyReservation');
Route::get('/reservation/{id}/edit', 'reservationController@edit');
Route::post('reservation/update', 'reservationController@updateReservation')->name('update.reservation');
//End Route Reservation

//Route Contact
Route::get('/allContact/{id}','contactController@allContact');
Route::get('/contact/{id}/edit', 'contactController@edit');
Route::get('/deleteContact/destroy/{id}', 'contactController@destroyContact');
//End Route Contact

//Route Commande
Route::get('/allCommande/{id}','commandeController@allCommande');
Route::get('/listeCommande/{id}','commandeController@listCommande');
Route::get('/deleteCommande/destroy/{id}', 'commandeController@destroyCommande');
Route::get('/commande/{id}/edit', 'commandeController@edit');
Route::post('commande/update', 'commandeController@updateCommande')->name('update.commande');
//End Route Commande
