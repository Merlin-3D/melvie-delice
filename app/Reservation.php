<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    //
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getStatusAttribute($attributes){

      return[
        '0' => 'Non Confirmer',
        '1' => 'Confirmer'
      ][$attributes];
    }
}
