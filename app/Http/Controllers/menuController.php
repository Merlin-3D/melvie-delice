<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Categorie;
use App\Validator;
use Illuminate\Http\Request;

use DataTables;

class menuController extends Controller
{
    //
    public function menu($id)
    {
      $categories = Categorie::where('user_id', '=', $id)->get();

      //dd($eleves);
     return view('controlCenter.menu', compact('categories'));
    }

    public function storeMenu(Request $request)
    {

           $image = $request->file('photo');

           $new_name = rand() . '.' . $image->getClientOriginalExtension();

           $image->move(public_path('imagesCenter'), $new_name);

           $form_data = array(
              'user_id'        =>  $request->user_id,
              'categorie_id'         =>  $request->categorie_id,
              'nom_menu' => $request->nom_menu,
              'description' => $request->description,
              'prix_moyen' => $request->prix_moyen,
              'photo' => $new_name,
          );

       Menu::create($form_data);

       return response()->json(['success'=>'Menu Ajouter']);

  }
  public function allMenu(Request $request, $id){

    if ($request->ajax()) {

        //$rechercher = $request->get('user_id');

        $data = Menu::latest()->where('user_id', '=', $id);
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                  $btn ='<button type="button"  class="btn btn-warning btn-sm edit" name="edit" id="'.$row->id.'"><i class="fas fa-edit"></i></button>';
                  $btn .= '&nbsp;&nbsp;';
                  $btn .='<button type="button"  class="btn btn-danger btn-sm delete" name="delete" id="'.$row->id.'"><i class="fas fa-trash"></i></button>';

                  return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
    }

    $categories = Categorie::All();
    return view('menu',compact('categories'));
  }

  public function destroyMenu($id)
  {
      $data = Menu::findOrFail($id);
      $data->delete();
  }
  public function edit($id)
 {
     if(request()->ajax())
     {
         $data = Menu::findOrFail($id);
         return response()->json(['result' => $data]);
     }
 }

 public function updateMenu(Request $request, Menu $menu)
 {

   $image = $request->file('photo');

   $new_name = rand() . '.' . $image->getClientOriginalExtension();

   $image->move(public_path('imagesCenter'), $new_name);


  $form_data = array(
     'user_id'        =>  $request->user_id,
     'categorie_id'         =>  $request->categorie_id,
     'nom_menu' => $request->nom_menu,
     'description' => $request->description,
     'prix_moyen' => $request->prix_moyen,
     'photo' => $new_name,
 );

  Menu::whereId($request->id)->update($form_data);

  return response()->json(['success' => 'Modification Reussit']);
 }


}
