<?php

namespace App\Http\Controllers;

use App\Slider;

use Illuminate\Http\Request;
use DataTables;

class sliderController extends Controller
{
    //
    public function storeSlider(Request $request)
    {

      $image = $request->file('photo');

      $new_name = rand() . '.' . $image->getClientOriginalExtension();

      $image->move(public_path('imagesSlider'), $new_name);

      $form_data = array(
         'user_id'        =>  $request->user_id,
         'titre'         =>  $request->titre,
         'sous_titre' => $request->sous_titre,
         'photo' => $new_name,
     );

    Slider::create($form_data);

    return response()->json(['success'=>'Slider Ajouter']);
    }

    public function allSlider(Request $request, $id){

      if ($request->ajax()) {

          //$rechercher = $request->get('user_id');

          $data = Slider::latest()->where('user_id', '=', $id);
          return Datatables::of($data)
                  ->addIndexColumn()
                  ->addColumn('action', function($row){

                    $btn ='<button type="button"  class="btn btn-warning btn-sm edit" name="edit" id="'.$row->id.'"><i class="fas fa-edit"></i></button>';
                    $btn .= '&nbsp;&nbsp;';
                    $btn .='<button type="button"  class="btn btn-danger btn-sm delete" name="delete" id="'.$row->id.'"><i class="fas fa-trash"></i></button>';

                    return $btn;
                  })
                  ->rawColumns(['action'])
                  ->make(true);
      }


      return view('slider');
    }

    public function destroySlider($id)
    {
        $data = Slider::findOrFail($id);
        $data->delete();
    }
    public function edit($id)
   {
       if(request()->ajax())
       {
           $data = Slider::findOrFail($id);
           return response()->json(['result' => $data]);
       }
   }

}
