<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class profileController extends Controller
{
    //

    public function updatePhoto(Request $request, User $user)
    {

      $image = $request->file('photo');

      $new_name = rand() . '.' . $image->getClientOriginalExtension();

      $image->move(public_path('imagesProfile'), $new_name);


     $form_data = array(

        'photo' => $new_name,
    );

     User::whereId($request->id)->update($form_data);

     return response()->json(['success' => 'Photo Modifier ']);
    }
}
