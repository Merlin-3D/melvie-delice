<?php

namespace App\Http\Controllers;

use App\Commande;
use Illuminate\Http\Request;
use DataTables;


class commandeController extends Controller
{
    //
    public function allCommande(Request $request, $id){

      if ($request->ajax()) {

          //$rechercher = $request->get('user_id');

          $data = Commande::latest()->where('user_id', '=', $id)
                                    ->where('status', '=', 0)->get();
          return Datatables::of($data)
                  ->addIndexColumn()
                  ->addColumn('action', function($row){

                    $btn ='<button type="submit"  class="btn btn-warning btn-sm confirm" name="confirm" id="'.$row->id.'"><i class="fas fa-check"></i></button>';
                    $btn .= '&nbsp;&nbsp;';
                    $btn .='<button type="button"  class="btn btn-danger btn-sm delete" name="delete" id="'.$row->id.'"><i class="fas fa-trash"></i></button>';

                    return $btn;
                  })
                  ->rawColumns(['action'])
                  ->make(true);
      }


      return view('commande');
    }

    public function listCommande(Request $request, $id){

      if ($request->ajax()) {

          //$rechercher = $request->get('user_id');

          $data = Commande::latest()->where('user_id', '=', $id)
                                    ->where('status', '=', 1)->get();
          return Datatables::of($data)
                  ->addIndexColumn()
                  
                  ->make(true);
      }


      return view('commande');
    }
    public function destroyCommande($id)
    {
        $data = Commande::findOrFail($id);
        $data->delete();
    }
    public function edit($id)
   {
       if(request()->ajax())
       {
           $data = Commande::findOrFail($id);
           return response()->json(['result' => $data]);
       }
   }
   public function updateCommande(Request $request, Commande $commande)
   {
     $form_data =  request()->validate(array(

        'status'     =>  'required',

    ));

       Commande::whereId($request->id)->update($form_data);

       return response()->json(['success' => 'Confirmation Effectué']);

   }
}
