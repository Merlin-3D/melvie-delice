<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use DataTables;

class contactController extends Controller
{

  public function allContact(Request $request, $id){

    if ($request->ajax()) {

        //$rechercher = $request->get('user_id');

        $data = Contact::latest()->where('user_id', '=', $id);
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                  $btn ='<button type="button"  class="btn btn-warning btn-sm look" name="look" id="'.$row->id.'"><i class="fas fa-list"></i></a></button>';
                  $btn .= '&nbsp;&nbsp;';
                  $btn .='<button type="button"  class="btn btn-danger btn-sm delete" name="delete" id="'.$row->id.'"><i class="fas fa-trash"></i></button>';

                  return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
    }


    return view('reservation');
  }
  public function destroyContact($id)
  {
      $data = Contact::findOrFail($id);
      $data->delete();
  }

  public function edit($id)
 {
     if(request()->ajax())
     {
         $data = Contact::findOrFail($id);
         return response()->json(['result' => $data]);
     }
 }

}
