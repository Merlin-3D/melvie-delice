<?php

namespace App\Http\Controllers;

use App\Categorie;

use Illuminate\Http\Request;
use DataTables;



class categorieCotroller extends Controller
{

    public function storeCategorie(Request $request)
    {

        $form_data  = request()->validate(array(
          'user_id'    =>  'required',
          'nom_categorie'     =>  'required',
        ));


        Categorie::create($form_data);

        return response()->json(['success' => 'catégorie Ajouter avec succés!.']);

    }

    public function allCategorie(Request $request, $id)
    {
        if ($request->ajax()) {

            //$rechercher = $request->get('user_id');

            $data = Categorie::latest()->where('user_id', '=', $id);
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                      $btn ='<button type="button"  class="btn btn-warning btn-sm edit" name="edit" id="'.$row->id.'"><i class="fas fa-edit"></i></button>';
                      $btn .= '&nbsp;&nbsp;';
                      $btn .='<button type="button"  class="btn btn-danger btn-sm delete" name="delete" id="'.$row->id.'"><i class="fas fa-trash"></i></button>';

                      return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        return view('categorie');
    }
    public function destroyCategorie($id)
    {
        $data = Categorie::findOrFail($id);
        $data->delete();
    }
    public function edit($id)
   {
       if(request()->ajax())
       {
           $data = Categorie::findOrFail($id);
           return response()->json(['result' => $data]);
       }
   }

    public function updateCategorie(Request $request, Categorie $categorie)
    {
      $form_data =  request()->validate(array(

         'nom_categorie'     =>  'required',

     ));

        Categorie::whereId($request->id)->update($form_data);

        return response()->json(['success' => 'Modification Reussit']);

    }
}
