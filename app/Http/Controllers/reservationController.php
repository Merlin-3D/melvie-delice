<?php

namespace App\Http\Controllers;

use App\Reservation;
use Illuminate\Http\Request;
use DataTables;

class reservationController extends Controller
{
    //
    public function allReservation(Request $request, $id){

      if ($request->ajax()) {

          //$rechercher = $request->get('user_id');

          $data = Reservation::latest()->where('user_id', '=', $id);
          return Datatables::of($data)
                  ->addIndexColumn()
                  ->addColumn('action', function($row){

                    $btn ='<button type="submit"  class="btn btn-warning btn-sm confirm" name="confirm" id="'.$row->id.'"><i class="fas fa-check"></i></button>';
                    $btn .= '&nbsp;&nbsp;';
                    $btn .='<button type="button"  class="btn btn-danger btn-sm delete" name="delete" id="'.$row->id.'"><i class="fas fa-trash"></i></button>';

                    return $btn;
                  })
                  ->rawColumns(['action'])
                  ->make(true);
      }


      return view('reservation');
    }
    public function destroyReservation($id)
    {
        $data = Reservation::findOrFail($id);
        $data->delete();
    }
    public function edit($id)
   {
       if(request()->ajax())
       {
           $data = Reservation::findOrFail($id);
           return response()->json(['result' => $data]);
       }
   }
   public function updateReservation(Request $request, Reservation $reservation)
   {
     $form_data =  request()->validate(array(

        'status'     =>  'required',

    ));

       Reservation::whereId($request->id)->update($form_data);

       return response()->json(['success' => 'Confirmation Effectué']);

   }

}
