<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class menu extends Model
{
    //
    protected $guarded = [];

    public function categorie()
    {
        return $this->belongsTo('App\Categorie');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
