<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone', 'email', 'password','usertype','name_restaurant','lieu','region','prix_moyen',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function categorie()
    {
         return $this->hasMany('App\Categorie');
    }
    public function menu()
    {
         return $this->hasMany('App\Menu');
    }
    public function slider()
    {
         return $this->hasMany('App\Slider');
    }
    public function reservation()
    {
         return $this->hasMany('App\Reservation');
    }
    public function contact()
    {
         return $this->hasMany('App\Contact');
    }
    public function commande()
    {
         return $this->hasMany('App\Commande');
    }
}
