<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categorie extends Model
{
    //
  //  protected $with = ['categorie','nom_categorie'];

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function menu()
    {
         return $this->hasMany('App\Menu');
    }
}
